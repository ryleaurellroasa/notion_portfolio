# Ryle Aurell Roasa

I am a software developer based in the Philippines. I am just starting with my career in the software development industry.

My focus area is in frontend web and mobile application development. I am currently using Flutter and Dart for mobile application development. I love learning new things and I always strive to become the best at what I do.

# Contact Information

LinkedIn: [www.linkedin.com/in/ryle-aurell-roasa](http://www.linkedin.com/in/ryle-aurell-roasa)

Website Portfolio: [https://roasa-flutter-portfolio.herokuapp.com/#/](https://roasa-flutter-portfolio.herokuapp.com/#/)

# Work Experience

## Flutter Developer Trainee

FFUF Manila, Inc., from July 2021 to Present

- I am currently training for mobile application development using Flutter and Dart.
- This training also involves OOP concepts, best coding practices and internal tools used in the organization.

## Mechanical Engineer

ArchEn Technologies, Inc., from December 2013 to April 2014

- I worked with the engineering team to design components for a cement plant.
- I used AutoCAD to design these components.

# Skills

## Technical Skills

- Web Development
- Software Engineering

## Soft Skills

- Creativity
- Teamwork
- Problem solving
- Analytical thinking
- Logical thinking

# Education

## BS Mechanical Engineering

Don Bosco Technical College, 2007 to 2012